package com.graphqlservertest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphqlservertestApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphqlservertestApplication.class, args);
	}

}
